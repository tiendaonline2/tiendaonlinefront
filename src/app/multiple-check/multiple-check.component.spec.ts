import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MultipleCheckComponent } from './multiple-check.component';

describe('MultipleCheckComponent', () => {
  let component: MultipleCheckComponent;
  let fixture: ComponentFixture<MultipleCheckComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [MultipleCheckComponent]
    });
    fixture = TestBed.createComponent(MultipleCheckComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
