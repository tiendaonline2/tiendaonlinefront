import { Component } from '@angular/core';
import {ProductService} from "../product.service";
import {MessageService} from "primeng/api";
export interface Product {
  id?:string;
  code?:string;
  name?:string;
  description?:string;
  price?:number;
  quantity?:number;
  inventoryStatus?:string;
  category?:string;
  image?:string;
  rating?:number;
}

@Component({
  selector: 'app-multiple-check',
  templateUrl: './multiple-check.component.html',
  styleUrls: ['./multiple-check.component.css']
})
export class MultipleCheckComponent {
  products!: Product[];

  selectedProduct1!: Product;

  selectedProduct2!: Product;

  selectedProduct3!: Product;

  selectedProducts1!: Product[];

  selectedProducts2!: Product[];

  selectedProducts3!: Product[];

  constructor(private productService: ProductService, private messageService: MessageService) { }

  ngOnInit() {
    this.productService.getProductsSmall().then(data => this.products = data);
  }

  selectProduct(product: Product) {
    this.messageService.add({severity:'info', summary:'Product Selected', detail: product.name});
  }

  onRowSelect(event: any) {
    this.messageService.add({severity:'info', summary:'Product Selected', detail: event.data.name});
  }

  onRowUnselect(event: any) {
    this.messageService.add({severity:'info', summary:'Product Unselected',  detail: event.data.name});
  }
}
