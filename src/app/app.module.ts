import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import {AccordionModule} from "primeng/accordion";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import { MultipleCheckComponent } from './multiple-check/multiple-check.component';
import {TableModule} from "primeng/table";
import {ButtonModule} from "primeng/button";
import {ToastModule} from "primeng/toast";
import {HttpClient, HttpClientModule, HttpHandler} from "@angular/common/http";
import {MessageService} from "primeng/api";

@NgModule({
  declarations: [
    AppComponent,
    MultipleCheckComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    /* -----------------------------------
       PRIME NG MODULES
    ------------------------------------- */
    AccordionModule,
    TableModule,
    ButtonModule,
    ToastModule,
  ],
  providers: [HttpClient, MessageService],
  bootstrap: [AppComponent]
})
export class AppModule { }
